FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

RUN apt install git python3 python3-pip python3-venv python3-virtualenv -y
RUN apt install automake make cmake automake bison flex g++ libboost-all-dev libevent-dev libssl-dev libtool make pkg-config -y

RUN pip3 install torch torchvision torchaudio

RUN python3 --version
RUN gcc --version

